<?php require_once("PHPMailerAutoload.php");
session_start();
$nome = $_POST["nome"];
$email = $_POST["email"];
$mensagem = $_POST["mensagem"];
$resposta = intval($_POST["resposta"]);

$nomeApp = "Minha Loja 2.0";
$emailWebmaster = "br.dev.apps@gmail.com";

if($resposta !== 11) {
    $_SESSION["danger"] = "Falha ao entrar em contato, resposta incorreta!";
    header("Location: contato.php");
    die();
}

$mail = new PHPMailer();
$mail->isSMTP();
$mail->Host = 'smtp.gmail.com';
$mail->Port = 587;
$mail->SMTPSecure = 'tls';
$mail->SMTPAuth = true;
$mail->Username = $emailWebmaster;
$mail->Password = "123456";
$mail->setFrom($emailWebmaster, $nomeApp);
$mail->AddCC($emailWebmaster, $nomeApp);
$mail->AddCC($email, $nome);
$mail->addReplyTo($emailWebmaster, $nomeApp);
$mail->Subject = "Email de contato da " . $nomeApp;
$mail->msgHTML("<html>de: {$nome}<br/>email: {$email}<br/>mensagem: {$mensagem}</html>");
$mail->AltBody = "de: {$nome}\nemail:{$email}\nmensagem: {$mensagem}";

if($mail->send()) {
    $_SESSION["success"] = "Mensagem enviada com sucesso";
    header("Location: index.php");
} else {
    $_SESSION["danger"] = "Erro ao enviar mensagem, tente novamente mais tarde!" . $mail->ErrorInfo;
    header("Location: contato.php");
}
die();
