<?php require_once("template/cabecalho.php"); ?>

<div class="container">
    <form class="form-horizontal" role="form" action="contato-envia-email.php" method="post">
        <fieldset>
            <legend>Entre em contato</legend>
            <div class="form-group">
                <label class="col-md-4 control-label" for="nome">Nome:</label>
                <div class="col-md-4 inputGroupContainer">
                    <div class="input-group">
                        <span class="input-group-addon"><i class="glyphicon glyphicon-user"></i></span>
                        <input id="nome" class="form-control" placeholder="Seu nome" type="text" name="nome" required="true" autofocus="true">
                    </div>
                </div>
            </div>
            <div class="form-group">
                <label class="col-md-4 control-label" for="email">Email:</label>
                <div class="col-md-4 inputGroupContainer">
                    <div class="input-group">
                        <span class="input-group-addon"><i class="glyphicon glyphicon-envelope"></i></span>
                        <input id="email" class="form-control" placeholder="Seu email" type="email" name="email" required="true">
                    </div>
                </div>
            </div>
            <div class="form-group">
                <label class="col-md-4 control-label" for="mensagem">Mensagem:</label>
                <div class="col-md-4 inputGroupContainer">
                    <div class="input-group">
                        <span class="input-group-addon">
                            <i class="glyphicon glyphicon-comment"></i>
                        </span>
                        <textarea id="mensagem" class="form-control" name="mensagem" placeholder="Sua mensagem" required="true" ></textarea>
                    </div>
                </div>
            </div>
            <div class="form-group">
                <label class="col-md-4 control-label" for="resposta">Quanto é 3 + 8?</label>
                <div class="col-md-4 inputGroupContainer">
                    <div class="input-group">
                        <span class="input-group-addon">
                            <i class="glyphicon glyphicon-education"></i>
                        </span>
                        <input id="resposta" class="form-control" placeholder="Sua resposta" type="number" name="resposta" required="true">
                    </div>
                </div>
            </div>
            <div class="form-group">
                <label class="col-md-4 control-label"></label>
                <div class="col-md-4">
                    <button type="submit" class="btn btn-primary" >
                        Enviar <span class="glyphicon glyphicon-send"></span>
                    </button>
                </div>
            </div>
        </fieldset>
    </form>
</div>

<?php require_once("template/rodape.php"); ?>
