<?php include("template/cabecalho.php"); ?>

<h1>Exercício 1</h1>

<p>Uma função que recebe um array de números. O retorno da função será a soma de todos os números de dentro do array.</p>

<?php

	//URL: http://localhost/loja/exe1.php?vetor[]=1&vetor[]=2&vetor[]=3&vetor[]=4&vetor[]=5

    // echo "true" === true ? "sim" : "nao"; //"true" eh uma string

	// $vetor = $_GET["vetor"];
    //
	// function somaArray($vetor) {
	// 	$resultado = 0;
	// 	/*
	// 	for($i = 0; $i < sizeof($vetor); $i++) {
	// 		$resultado += $vetor[$i];
	// 	}
	// 	*/
	// 	foreach ($vetor as $valor) {
	// 		$resultado += $valor;
	// 	}
	// 	return $resultado;
	// }
    //
	// echo "Resultado da soma: " . somaArray($vetor);
?>

<?php include("template/rodape.php"); ?>
