<?php require_once("template/cabecalho.php");
require_once("seguranca.php");
?>

    <h1>Bem Vindo!</h1>

    <?php if(usuarioAutenticado()) { ?>
        <p class="text-success">Olá <?=getUsuarioAutenticado()?></p>

        <?php }  else {?>
            <div class="container">
                <div class="row">
                    <div class="col-sm-6 col-sm-offset-3 form-box">
                        <div class="form-top">
                            <div class="form-top-left">
                                <h2>Login</h2>
                            </div>
                            <div class="form-top-right">
                                <i class="fa fa-key"></i>
                            </div>
                        </div>
                        <div class="form-bottom">
                            <form class="login-form" action="login.php" method="post">
                                <fieldset>
                                    <div class="form-group">
                                        <label class="form-control-label" for="email">Email:</label>
                                        <input class="form-control form-username" id="email" type="email" name="email" placeholder="Informe seu email"
                                            required="true" autofocus="true" />
                                        <spam id="emailHelpBlock" class="help-block text-left">E-mail padrão: bruno.ti.es@gmail.com</spam>
                                    </div>
                                    <div class="form-group">
                                        <label class="form-control-label" for="senha">Senha:</label>
                                        <input class="form-control" id="senha" type="password" placeholder="Informe sua senha"
                                        name="senha" required="true" />
                                        <span id="senhaHelpBlock" class="help-block text-left">Senha padrão: 123456</spam>
                                    </div>
                                    <button class="btn btn-primary">Entrar</button>
                                </fieldset>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <?php } ?>

        <?php include("template/rodape.php"); ?>
