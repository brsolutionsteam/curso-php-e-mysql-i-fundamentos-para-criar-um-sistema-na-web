<?php require_once("usuario-controller.php");
require_once("seguranca.php");

$usuario = buscaUsuarioPorEmail($conexao, $_POST["email"], $_POST["senha"]);

if($usuario == null) {
    $_SESSION["danger"] = "Usuário ou senha inválido!";
    header("Location: index.php"); // toda string eh true, soh eh false 0 ou "" vazio
} else {
    logaUsuario($usuario["email"]);
    $_SESSION["success"] = "Usuário autenticado com sucesso.";
    header("Location: index.php");
}

// var_dump($usuario); // funcao utilizada para exibicao de array

die();
