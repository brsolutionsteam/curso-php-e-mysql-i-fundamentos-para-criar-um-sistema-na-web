<?php require_once("template/cabecalho.php");
	require_once("produto-controller.php");
    require_once ("seguranca.php");

    verificaUsuario();
?>

	<?php

        $id = $_POST["id"];
		$nome = $_POST["nome"];
		$preco = $_POST["preco"];
		$descricao = $_POST["descricao"];
        $categoria_id = $_POST["categoria_id"];

        if(array_key_exists('usado', $_POST)) {
            $usado = "true";
        } else {
            //$usado = false; false é uma string vazia
            $usado = "false";
        }

		if(alteraProduto($id, $nome, $preco, $descricao, $usado, $categoria_id, $conexao)) {
	?>
			<p class="text-success">
				<?php
					echo "Produto " . $nome . ", " . $preco . " alterado com sucesso!";
				?>
			</p>

	<?php
		} else {
	?>
			<p class="text-danger">
				<?php
					$msgErro = mysqli_error($conexao);
					echo "Falha ao tentar alterar o Produto " . $nome . "! " . $msgErro;
				?>
			</p>
	<?php
		}

		mysqli_close($conexao);

	?>


<?php include("template/rodape.php"); ?>
