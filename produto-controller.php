<?php 	require_once("conecta.php");

function insereProduto($nome, $preco, $descricao, $usado, $categoria_id, $conexao) {
    $nome = mysqli_real_escape_string($conexao, $nome);
    $preco = mysqli_real_escape_string($conexao, $preco);
    $descricao = mysqli_real_escape_string($conexao, $descricao);
    $query = "insert into produtos (nome, preco, descricao, usado, categoria_id)
    values('{$nome}', {$preco}, '{$descricao}', {$usado}, {$categoria_id})";
    // echo $query; debugando o insert
    // var_dump($query);
    return mysqli_query($conexao, $query);
}

function alteraProduto($id, $nome, $preco, $descricao, $usado, $categoria_id, $conexao) {
    $nome = mysqli_real_escape_string($conexao, $nome);
    $preco = mysqli_real_escape_string($conexao, $preco);
    $descricao = mysqli_real_escape_string($conexao, $descricao);
    $query = "update produtos set nome = '{$nome}', preco = {$preco}, descricao = '{$descricao}',
        usado = {$usado}, categoria_id = {$categoria_id} where id = {$id}";
    //echo $query; //debugando a query
    // var_dump($query);
    return mysqli_query($conexao, $query);
}

function listaProdutos($conexao) {
    $produtos = array(); // []; essa sintaxe soh funciona nas versoes mais novas do PHP
    $query = "select p.*, c.nome as categoria_nome from produtos as p join categorias as c on c.id = p.categoria_id order by p.id asc";
    $resultadoQuery = mysqli_query($conexao, $query);

    while($produto = mysqli_fetch_assoc($resultadoQuery)) {
        array_push($produtos, $produto);
    }
    return $produtos;
}

function buscaProdutoPorId($conexao, $id) {
    $query = "select * from produtos where id = {$id}";
    $resultadoQuery = mysqli_query($conexao, $query);
    return mysqli_fetch_assoc($resultadoQuery);
}

function removeProduto($conexao, $id) {
    $query = "delete from produtos where id = {$id}";
    return mysqli_query($conexao, $query);
}

?>
