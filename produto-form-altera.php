<?php require_once("template/cabecalho.php");
    require_once ("produto-controller.php");
    require_once ("categoria-controller.php");
    require_once("seguranca.php");

    verificaUsuario();

    $id = $_GET['id'];
    $produto = buscaProdutoPorId($conexao, $id);
    $categorias = listaCategorias($conexao);
    $usado = $produto['usado'] ? "checked='checked'" : "";
?>

<h1>Cadastro de Produto</h1>

<form action="produto-altera.php" method="post">
    <input type="hidden" name="id" value="<?=$produto['id']?>"
    <table class="table">
        <?php include("produto-form-base.php"); ?>
    </table>
</form>

<?php include("template/rodape.php"); ?>
