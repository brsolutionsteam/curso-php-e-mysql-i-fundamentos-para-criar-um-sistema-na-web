        <tr>
            <td>Nome:</td>
            <td><input class="form-control" type="text" name="nome" value="<?=$produto['nome']?>" required="true"></td>
        </tr>
        <tr>
            <td>Preço:</td>
            <td><input class="form-control" type="number" name="preco" value=<?=$produto['preco']?> ></td>
        </tr>
        <tr>
            <td>Descrição:</td>
            <td><textarea class="form-control" name="descricao"><?=$produto['descricao']?></textarea> </td>
        </tr>
        <tr>
            <td></td>
            <!-- IMPORTANTE checkbox nao marcado nao eh enviado -->
            <td><input type="checkbox" name="usado" value="false" <?=$usado?>>Usado?</td>
        </tr>
        <tr>
            <td>Categoria:</td>
            <td>
                <select name="categoria_id" class="form-control">
                <?php foreach ($categorias as $categoria) :
                    $essaEhCategoria = $produto['categoria_id'] == $categoria['id'];
                    $selecao = $essaEhCategoria ? "selected = 'selected'" : "";
                ?>
                    <option value="<?=$categoria['id']?>" <?=$selecao?>>
                        <?=$categoria['nome']?>
                    </option> >
                <?php endforeach ?>
            </td>
        </tr>
        <tr>
            <td><input class="btn btn-primary" type="submit" value="Salvar"></td>
        </tr>
