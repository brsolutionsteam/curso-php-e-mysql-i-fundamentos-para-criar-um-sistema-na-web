<?php require_once("template/cabecalho.php");
require_once ("categoria-controller.php");
require_once ("seguranca.php");

verificaUsuario();

$produto = array("nome" => "", "descricao" => "", "preco" => "", "categoria_id" => "1");
$usado = "";
$categorias = listaCategorias($conexao);

?>

<h1>Cadastro de Produto</h1>

<form action="produto-adiciona.php" method="post">
    <table class="table">
        <?php include("produto-form-base.php"); ?>
        </table>
    </form>

    <?php include("template/rodape.php"); ?>
