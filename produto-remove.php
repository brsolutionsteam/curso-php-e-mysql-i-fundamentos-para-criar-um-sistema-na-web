<?php require_once("template/cabecalho.php");
require_once("produto-controller.php");
require_once("seguranca.php");

verificaUsuario();

$id = $_POST['id'];

removeProduto($conexao, $id);
$_SESSION["success"] = "Produto removido com sucesso.";
header("Location: produtos.php");

die();
?>
