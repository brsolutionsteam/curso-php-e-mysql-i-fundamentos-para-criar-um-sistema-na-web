<?php require_once("template/cabecalho.php");
require_once("produto-controller.php");
require_once("seguranca.php");
?>
<div class="page-header">
    <h1>Produtos</h1>
</div>
<div class="row">
    <table class="table table table-hover">
        <thead>
            <th>ID</th>
            <th>Nome</th>
            <th>Preço</th>
            <th>Descrição</th>
            <th>Categoria</th>
        </thead>
        <tbody>
            <?php

            $produtos = listaProdutos($conexao);

            foreach ($produtos as $produto) :
                ?>
                <tr>
                    <td><?=$produto['id']?></td>
                    <td><?=$produto['nome']?></td>
                    <td><?=$produto['preco']?></td>
                    <td><?=substr($produto['descricao'], 0, 15) . "..."?></td>
                    <td><?=$produto['categoria_nome']?></td>
                    <td>
                        <a class="btn btn-primary" href="produto-form-altera.php?id=<?=$produto['id']?>">Alterar</a>
                        <!-- <form action="produto-form.php?id=<?=$produto['id']?>" method="get">
                        <button class="btn btn-primary">Alterar</button>
                    </form> -->
                </td>
                <td>
                    <form action="produto-remove.php" method="post">
                        <input type="hidden" name="id" value="<?=$produto['id']?>">
                        <button class="btn btn-danger">Remover</button>
                    </form>
                </td>
            </tr>

            <?php
            endforeach

            ?>
        </tbody>
    </table>
    <div class="row">
        <?php include("template/rodape.php"); ?>
