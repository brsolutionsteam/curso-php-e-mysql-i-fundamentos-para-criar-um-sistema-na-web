<?php

session_start();

function verificaUsuario() {
    if(!usuarioAutenticado()) {
        $_SESSION["danger"] = "Você precisa se autenticar para acessar este recurso!";
        header("Location: index.php");
        die();
    }
}

function usuarioAutenticado() {
    // return isset($_COOKIE["usuario_autenticado"]);
    return isset($_SESSION["usuario_autenticado"]);
}

function getUsuarioAutenticado() {
    return $_SESSION["usuario_autenticado"];
}

function logaUsuario($email) {
    // setcookie("usuario_autenticado", $email);
    $_SESSION["usuario_autenticado"] = $email;
}

function logout() {
    session_destroy();
    session_start();
}
?>
