<?php require_once("template/cabecalho.php");?>

<div class="page-header">
    <h1>Desenvolvedor: Bruno Silva</h1>
</div>
<div class="text-left">
    Trabalho na área de Tecnologia da Informação (TI) desde 2002, sou graduando em Sistemas de Informação,
    oitavo período, sempre com foco nas necessidades dos clientes. <br /><br />
    Sou altamente comprometido com resultado, acredito que o problema de um
    membro do time é um problema do time e prefiro discutir soluções ao invés de problemas.<br /><br />
    Busco excelência no atendimento aos clientes internos e externos e nas soluções com o melhor custo benefício,
    sempre alinhando à TI aos objetivos do negócio, agregando valor às atividades.<br /><br />
    Desde 2010 venho me especializando em Análise e Desenvolvimento de Software.<br /><br />
    Gosto muito da área de desenvolvimento, trabalho em equipe, desafios, aprender, servir e me desenvolver tanto com
    pessoa quanto como profissional.<br /><br />
    <strong>As principais metodologias/tecnologias que trabalho são:</strong>
    <ul class="text-left">
        <li><strong>Gerenciamento de Projeto</strong></li>
        <ul>
            <li>Scrum</li>
            <li>PMI</li>
        </ul>
        <li><strong>Modelagem</strong></li>
        <ul>
            <li>UML</li>
            <li>BPMN</li>
            <li>Fluxograma</li>
        </ul>
        <li><strong>Tecnologias</strong></li>
        <ul>
            <li>Java</li>
            <ul>
                <li>JavaEE</li>
                <li>JSF</li>
                <li>JPA</li>
                <li>Hibernate</li>
                <li>Maven</li>
                <li>JasperReports</li>
                <li>Primefaces</li>
            </ul>
            <li>PHP</li>
            <li>SQL</li>
            <li>HTML</li>
            <li>CSS</li>
            <li>Javascript</li>
            <li>HTTP</li>
            <li>Linux</li>
            <li>Git</li>
        </ul>
    </ul>
    <address>
        <strong>contato: </strong><a href="mailto:#">bruno.ti.es@gmail.com</a>
        </address

    </div>
    <?php include("template/rodape.php"); ?>
