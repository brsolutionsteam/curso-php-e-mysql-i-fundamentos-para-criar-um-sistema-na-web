<?php require_once("mostra-alerta.php");
error_reporting(E_ALL ^ E_NOTICE); //exibindo todas as mensagens do PHP menos as de notice
session_start();

?>
<html>
<head>
    <meta charset="utf-8">
    <link rel="stylesheet" type="text/css" href="css/bootstrap.css">
    <link rel="stylesheet" type="text/css" href="css/loja.css">
    <title>Minha Loja 1.0</title>
</head>
<body>

    <div class="navbar navbar-inverse navbar-fixed-top">
        <div class="container">
            <div class="navbar-header">
                <a class="navbar-brand" href="index.php">Minha Loja</a>
            </div>
            <div>
                <ul class="nav navbar-nav">
                    <li><a href="produto-form.php">Adiciona Produto</a></li>
                    <li><a href="produtos.php">Produtos</a></li>
                    <li><a href="sobre.php">Sobre</a></li>
                    <li><a href="contato.php">Contato</a></li>
                    <li><a href="logout.php">Sair</a></li>
                </ul>
            </div>
        </div>
    </div>

    <div class="container">
        <div class="principal">

            <?php
                mostraAlerta("success");
                mostraAlerta("danger");
            ?>
